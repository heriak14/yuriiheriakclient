package com.epam.service.soap;

import com.epam.client.Client;
import com.epam.client.soap.SOAPClient;
import com.epam.model.Mail;
import com.epam.utils.DateFormatter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;

import static org.testng.Assert.*;

public class MailServiceTest {
    private static Logger logger = LogManager.getLogger(MailServiceTest.class);
    private Client client;
    private Mail testMail;

    @BeforeClass
    private void setUp() {
        logger.info("<------------------SOAP SERVICE TESTS--------------------->");
        client = new SOAPClient();
        testMail = new Mail();
        testMail.setSenderEmail("sender@ukr.net");
        testMail.setReceiverEmail("receiver@gmail.com");
        testMail.setSubject("subject");
        testMail.setBody("body");
        testMail.setDate(DateFormatter.dateToString(LocalDateTime.now()));
    }

    @BeforeMethod
    public void testSendMail() throws IOException {
        logger.info("test sending mail");
        Mail[] mail = client.getAllMails();
        if (mail.length > 0) {
            testMail.setId(mail[0].getId());
            assertFalse(client.sendMail(testMail), "mail shouldn't have been sent");
        }
        int maxId = Arrays.stream(mail).mapToInt(Mail::getId).max().orElse(0) + 1;
        testMail.setId(maxId);
        assertTrue(client.sendMail(testMail), "mail was not sent");
    }

    @Test
    public void testGetAllMails() throws IOException {
        logger.info("test getting all mail");
        Mail[] allMail = client.getAllMails();
        assertNotNull(allMail);
        assertTrue(allMail.length > 0, "service must contain at least 1 mail");
        assertTrue(Arrays.asList(allMail).contains(testMail), "service don't contains test mail");
    }

    @AfterMethod
    public void testDeleteMail() {
        logger.info("test deleting mail");
        assertTrue(client.deleteMail(testMail.getId()), "mail was not deleted");
        assertFalse(client.deleteMail(testMail.getId() + 1), "mail shouldn't have been deleted");
    }

    @Test
    public void testGetMailsBySender() throws IOException {
        logger.info("test getting mail by sender email");
        Mail[] mail = client.getMailsBySender(testMail.getSenderEmail());
        assertNotNull(mail);
        assertTrue(mail.length > 0, "service must contain at least 1 mail");
        assertTrue(Arrays.asList(mail).contains(testMail), "service don't contains test mail");
    }

    @Test
    public void testGetMailsBySubject() throws IOException {
        logger.info("test getting mail by sender email");
        Mail[] mail = client.getMailsBySubject(testMail.getSubject());
        assertNotNull(mail);
        assertTrue(mail.length > 0, "service must contain at least 1 mail");
        assertTrue(Arrays.asList(mail).contains(testMail), "service don't contains test mail");
    }
}