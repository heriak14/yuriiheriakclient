package com.epam.service.rest;

import com.epam.consts.Endpoint;
import com.epam.model.Mail;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

class AssuredQueries {
    static {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(Endpoint.BASE_URI)
                .setPort(Endpoint.PORT)
                .setBasePath(Endpoint.BASE_PATH)
                .build();
    }

    static void testPing() {
        getMailByEndpoint(Endpoint.GET_ALL_MAIL);
    }

    static Mail[] getMailByEndpoint(String endPoint) {
        return given().
                when().get(endPoint).
                then().statusCode(200).
                and().contentType(ContentType.JSON).
                extract().body().as(Mail[].class);
    }

    static void sendMail(Mail mail, int expectedStatusCode) {
        given().contentType(ContentType.JSON).body(mail).
                when().put(Endpoint.SEND_MAIL).
                then().statusCode(expectedStatusCode);
    }

    static void deleteMail(int id, int expectedStatusCode) {
        given().queryParam("id", id).
                when().delete(Endpoint.DELETE_MAIL).
                then().statusCode(expectedStatusCode);
    }
}
