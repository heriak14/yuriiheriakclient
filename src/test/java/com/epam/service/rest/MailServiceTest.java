package com.epam.service.rest;

import com.epam.consts.Endpoint;
import com.epam.model.Mail;
import com.epam.utils.DateFormatter;
import io.restassured.RestAssured;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Arrays;

import static com.epam.service.rest.AssuredQueries.*;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class MailServiceTest {
    private static Logger logger = LogManager.getLogger(MailServiceTest.class);
    private Mail testMail;

    @BeforeClass
    private void setUp() {
        logger.info("<------------------REST SERVICE TESTS--------------------->");
        RestAssured.basePath = Endpoint.BASE_PATH;
        testMail = new Mail();
        testMail.setSenderEmail("sender@ukr.net");
        testMail.setReceiverEmail("receiver@gmail.com");
        testMail.setSubject("subject");
        testMail.setBody("body");
        testMail.setDate(DateFormatter.dateToString(LocalDateTime.now()));
    }

    @Test
    private void testPing() {
        AssuredQueries.testPing();
    }


    @BeforeMethod
    private void testSendingMail() {
        Mail[] mail = getMailByEndpoint(Endpoint.GET_ALL_MAIL);
        if (mail.length > 0) {
            testMail.setId(mail[0].getId());
            sendMail(testMail, 404);
        }
        int id = Arrays.stream(mail)
                .mapToInt(Mail::getId)
                .max()
                .orElse(0) + 1;
        testMail.setId(id);
        sendMail(testMail, 200);
    }

    @Test
    private void testGettingAllMail() {
        Mail[] mail = getMailByEndpoint(Endpoint.GET_ALL_MAIL);
        assertNotNull(mail);
        assertTrue(mail.length > 0, "service must contain at least 1 mail");
        assertTrue(Arrays.asList(mail).contains(testMail), "service don't contains test mail");
    }


    @AfterMethod
    private void testDeletingMail() {
        logger.info("test deleting mail");
        deleteMail(testMail.getId(), 200);
        deleteMail(testMail.getId() + 1, 404);
    }

    @Test
    private void testGettingMailBySenderEmail() {
        Mail[] mail = getMailByEndpoint(String.format(Endpoint.GET_MAIL_BY_SENDER, testMail.getSenderEmail()));
        assertNotNull(mail);
        assertTrue(mail.length > 0, "service must contain at least 1 mail");
        assertTrue(Arrays.asList(mail).contains(testMail), "service don't contains test mail");
    }

    @Test
    private void testGettingMailBySubject() {
        Mail[] mail = getMailByEndpoint(String.format(Endpoint.GET_MAIL_BY_SUBJECT, testMail.getSubject()));
        assertNotNull(mail);
        assertTrue(mail.length > 0, "service must contain at least 1 mail");
        assertTrue(Arrays.asList(mail).contains(testMail), "service don't contains test mail");
    }
}
