package com.epam.model;

public enum Realization {
    SOAP, REST
}
