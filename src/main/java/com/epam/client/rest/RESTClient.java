package com.epam.client.rest;

import com.epam.consts.Endpoint;
import com.epam.client.Client;
import com.epam.model.Mail;
import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.MappingJsonFactory;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

public class RESTClient implements Client {

    @Override
    public boolean sendMail(Mail mail) {
        WebClient client = WebClient.create(Endpoint.BASE_PATH + Endpoint.SEND_MAIL, Collections.singletonList(new JacksonJsonProvider()));
        Response response = client.accept("application/json").type("application/json").put(mail);
        return response.getStatus() == Response.Status.OK.getStatusCode();
    }

    @Override
    public Mail[] getAllMails() throws IOException {
        return getMailByURL(Endpoint.GET_ALL_MAIL);
    }

    @Override
    public boolean deleteMail(int id) {
        WebClient client = WebClient.create(String.format(Endpoint.BASE_PATH + Endpoint.DELETE_MAIL, id));
        Response response = client.accept("application/json").delete();
        return response.getStatus() == Response.Status.OK.getStatusCode();
    }

    @Override
    public Mail[] getMailsBySender(String senderEmail) throws IOException {
        return getMailByURL(String.format(Endpoint.GET_MAIL_BY_SENDER, senderEmail));
    }

    @Override
    public Mail[] getMailsBySubject(String subject) throws IOException {
        return getMailByURL(String.format(Endpoint.GET_MAIL_BY_SUBJECT, subject));
    }

    private Mail[] getMailByURL(String url) throws IOException {
        WebClient client = WebClient.create(Endpoint.BASE_PATH + url);
        Response response = client.accept("application/json").get();
        if (response.getStatus() != Response.Status.OK.getStatusCode()) {
            return null;
        }
        MappingJsonFactory factory = new MappingJsonFactory();
        JsonParser parser = factory.createJsonParser((InputStream)response.getEntity());
        return parser.readValueAs(Mail[].class);
    }
}
