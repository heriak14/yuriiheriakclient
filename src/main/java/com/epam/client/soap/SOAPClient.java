package com.epam.client.soap;

import com.epam.client.Client;
import com.epam.model.Mail;
import com.epam.service.soap.MailService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

public class SOAPClient implements Client {
    private Optional<MailService> mailService;

    public SOAPClient() {
        mailService = initService();
    }

    private Optional<MailService> initService() {
        try {
            URL wsdlUrl = new URL("http://localhost:5554/mailservicerest/mailservice?wsdl");
            QName qName = new QName("http://soap.service.epam.com/", "MailServiceImplService");
            Service service = Service.create(wsdlUrl, qName);
            return Optional.ofNullable(service.getPort(MailService.class));
        } catch (MalformedURLException e) {
            return Optional.empty();
        }
    }

    @Override
    public boolean sendMail(Mail mail) {
        return mailService.map(service -> service.sendMail(mail)).orElse(false);
    }

    @Override
    public Mail[] getAllMails() {
        return mailService.map(MailService::getAllMails).orElse(new Mail[]{});
    }

    @Override
    public boolean deleteMail(int id) {
        return mailService.map(service -> service.deleteMail(id)).orElse(false);
    }

    @Override
    public Mail[] getMailsBySender(String senderEmail) {
        return mailService.map(service -> service.getMailsBySender(senderEmail)).orElse(new Mail[]{});
    }

    @Override
    public Mail[] getMailsBySubject(String subject) {
        return mailService.map(service -> service.getMailsBySubject(subject)).orElse(new Mail[]{});
    }
}
