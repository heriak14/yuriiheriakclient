package com.epam.client;

import com.epam.client.rest.RESTClient;
import com.epam.client.soap.SOAPClient;
import com.epam.model.Realization;

public class ClientFactory {

    public static Client getClient(Realization realization) {
        if (realization == Realization.SOAP) {
            return new SOAPClient();
        } else if (realization == Realization.REST) {
            return new RESTClient();
        } else {
            return null;
        }
    }

}
