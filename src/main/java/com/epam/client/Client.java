package com.epam.client;

import com.epam.model.Mail;
import com.epam.service.soap.MailService;

import java.io.IOException;

public interface Client {

    boolean sendMail(Mail mail);

    Mail[] getAllMails() throws IOException;

    boolean deleteMail(int id);

    Mail[] getMailsBySender(String senderEmail) throws IOException;

    Mail[] getMailsBySubject(String subject) throws IOException;
}
