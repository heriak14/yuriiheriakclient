package com.epam.view.impl;

import com.epam.client.ClientFactory;
import com.epam.model.Realization;
import com.epam.view.Executor;
import com.epam.view.View;

import java.util.LinkedHashMap;
import java.util.Map;

public class MainView implements View {
    private Map<String, String> menu;
    private Map<String, Executor> menuMethods;

    public MainView() {
        menu = setMenu();
        menuMethods = setMethods();
    }

    private Map<String, Executor> setMethods() {
        Map<String, Executor> mainMethods = new LinkedHashMap<>();
        mainMethods.put("1", this::startSOAPClient);
        mainMethods.put("2", this::startRESTClient);
        mainMethods.put("Q", this::quit);
        return mainMethods;
    }

    private Map<String, String> setMenu() {
        Map<String, String> menu = new LinkedHashMap<>();
        menu.put("1", "Start SOAP client");
        menu.put("2", "Start REST client");
        menu.put("Q", "Quit");
        return menu;
    }

    private void startRESTClient() {
        new ServiceView(ClientFactory.getClient(Realization.REST)).show();
    }

    private void startSOAPClient() {
        new ServiceView(ClientFactory.getClient(Realization.SOAP)).show();
    }

    private void quit() {
    }


    @Override
    public void show() {
        show(menu, menuMethods);
    }
}
