package com.epam.view.impl;

import com.epam.client.Client;
import com.epam.model.Mail;
import com.epam.utils.DateFormatter;
import com.epam.view.Executor;
import com.epam.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

public class ServiceView implements View {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private Map<String, String> mainMenu;
    private Map<String, Executor> mainMethods;
    private Client client;

    ServiceView(Client client) {
        mainMenu = setMenu();
        mainMethods = setMethods();
        this.client = client;
    }

    private Map<String, Executor> setMethods() {
        Map<String, Executor> mainMethods = new LinkedHashMap<>();
        mainMethods.put("1", this::sendMail);
        mainMethods.put("2", this::getAllMails);
        mainMethods.put("3", this::deleteMail);
        mainMethods.put("4", this::getMailBySender);
        mainMethods.put("5", this::getMailBySubject);
        mainMethods.put("Q", this::quit);
        return mainMethods;
    }

    private Map<String, String> setMenu() {
        Map<String, String> menu = new LinkedHashMap<>();
        menu.put("1", "Send mail");
        menu.put("2", "Get all mail");
        menu.put("3", "Delete mail");
        menu.put("4", "Get mail by sender email");
        menu.put("5", "Get mail by subject");
        menu.put("Q", "Quit");
        return menu;
    }

    private void sendMail() {
        Mail mail = enterMail();
        client.sendMail(mail);
    }

    private Mail enterMail() {
        Mail mail = new Mail();
        LOGGER.info("Sender: ");
        mail.setSenderEmail(SCANNER.nextLine());
        LOGGER.info("Receiver: ");
        mail.setReceiverEmail(SCANNER.nextLine());
        LOGGER.info("Subject: ");
        mail.setSubject(SCANNER.nextLine());
        LOGGER.info("Enter message:\n");
        mail.setBody(SCANNER.nextLine());
        mail.setDate(DateFormatter.dateToString(LocalDateTime.now()));
        return mail;
    }

    private void getAllMails() {
        try {
            Mail[] mail = client.getAllMails();
            LOGGER.info("--------------------Your mail------------------------\n");
            Arrays.stream(mail).forEach(LOGGER::info);
            LOGGER.info("------------------------------------------------------");
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void deleteMail() {
        LOGGER.info("Enter mail id to delete: ");
        String input = SCANNER.nextLine();
        if (!input.matches("^[0-9]+$")) {
            return;
        }
        client.deleteMail(Integer.parseInt(input));
    }

    private void getMailBySender() {
        LOGGER.info("Enter sender email: ");
        String sender = SCANNER.nextLine();
        try {
            Mail[] mail = client.getMailsBySender(sender);
            LOGGER.info("--------------------" + sender + "'s mail------------------------\n");
            Arrays.stream(mail).forEach(LOGGER::info);
            LOGGER.info("------------------------------------------------------");
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void getMailBySubject() {
        LOGGER.info("Enter subject: ");
        String subject = SCANNER.nextLine();
        try {
            Mail[] mail = client.getMailsBySubject(subject);
            LOGGER.info("--------------------Mail with subject: " + subject + "------------------------\n");
            Arrays.stream(mail).forEach(LOGGER::info);
            LOGGER.info("------------------------------------------------------");
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void quit() {
    }

    public void show() {
        show(mainMenu, mainMethods);
    }
}
