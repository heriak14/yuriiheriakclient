package com.epam.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.Scanner;

public interface View {
    Logger LOGGER = LogManager.getLogger();
    Scanner SCANNER = new Scanner(System.in);

    void show();

    default void printMenu(Map<String, String> menu) {
        LOGGER.info("_______________________"
                + "MAIN MENU______________________\n");
        for (Map.Entry entry : menu.entrySet()) {
            LOGGER.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        LOGGER.info("____________________________________________\n");
    }


    default void show(Map<String, String> menu, Map<String, Executor> methods) {
        String option;
        do {
            printMenu(menu);
            LOGGER.info("Choose one option: ");
            option = SCANNER.nextLine().toLowerCase().trim();
            if (methods.containsKey(option)) {
                methods.get(option).execute();
            } else {
                LOGGER.info("Wrong input! Try again.");
            }
        } while (!option.equals("q"));
    }
}
