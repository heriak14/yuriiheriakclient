package com.epam.view;

@FunctionalInterface
public interface Executor {
    void execute();
}
