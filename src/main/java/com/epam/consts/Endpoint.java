package com.epam.consts;

public final class Endpoint {
    public static final String BASE_URI = "http://localhost";
    public static final int PORT = 5554;
    public static final String BASE_PATH = "/mailservicerest/mail/service";
    public static final String SEND_MAIL = "/send";
    public static final String GET_ALL_MAIL = "/mail";
    public static final String DELETE_MAIL = "/delete?id=%s";
    public static final String GET_MAIL_BY_SENDER = "/sender?sender=%s";
    public static final String GET_MAIL_BY_SUBJECT = "/subject?subject=%s";
    private Endpoint() {
    }
}
