package com.epam.service.soap;

import com.epam.model.Mail;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface MailService {

    @WebMethod
    boolean sendMail(Mail mail);

    @WebMethod
    Mail[] getAllMails();

    @WebMethod
    boolean deleteMail(int id);

    @WebMethod
    Mail[] getMailsBySender(String senderEmail);

    @WebMethod
    Mail[] getMailsBySubject(String subject);
}
